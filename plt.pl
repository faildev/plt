use strict;
use warnings;

use Getopt::Std;
use vars qw($opt_c $opt_s);

use YAML qw 'LoadFile DumpFile';
use JSON::XS;


sub home
{
	return $ENV{HOME}        if $ENV{HOME};
	return $ENV{USERPROFILE} if $ENV{USERPROFILE};
	return  "";
}


my $configfile = home . '/.plt/config.yml';
my $configsection = '';
my $format = 'yml';
my $output = home .'/plt_output';

sub plthelp
{
	print "\nplt (c) 2011 mauro pizzamiglio\n";
	print "\nUsage: plt.pl [-c configfile] -s sectionname -f fromat [-o output]\n\n";
	print "\tformat: json, yml\n\n";
	print "plt is a perl log translator. The translation is guided by\n";
	print "a config file (in yaml format), one in your home directory\n";
	print "is the default (such as an example) which you can modify,\n";
	print "otherwise you can specify one by -c options.\n";
	die "\n";
}


sub init
{
	my %options=();
	getopts('c:s:f:o:m:', \%options);

	if(!$options{s})
	{
		plthelp;
	} else {
		$configsection = $options{s};
	}
	
	if(!$options{f})
	{
		plthelp;
	} else {
		$format = $options{f};
	}

	$configfile  = $options{c} if $options{c};
	$output  = $options{o} if $options{o};

	open my $fh, '<', $configfile
	  or die "can't open config file $configfile: $!";


	my $config = LoadFile $fh;
	return my $data = $config->{$configsection};
}

my $data = init;
my @missing = ();
my @translations = ();

open (LOG, $data->{log});
while(<LOG>)
{
	my %translation = ();
	my @result = ($_ =~  /$data->{regex}/);
	if(@result == @{$data->{out}})
	{
		
		for my $out (@{$data->{out}})
		{
			if($out->{show})
			{
				$translation{entry}{$out->{label}} = $result[$out->{position}];
			}
		}
	} else {
		push @missing, $_;
	}
	push @translations, \%translation;
}
close(LOG);

push @translations, @missing if($data->{missing});

if($format eq 'yml')
{
	DumpFile("$output.$format", \@translations);
} elsif($format eq 'json') {
	my $json = JSON::XS->new();
	open OUT, ">> $output.$format";
	print OUT $json->encode(\@translations)."\n";
	close OUT;
}
